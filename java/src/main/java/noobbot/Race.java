package noobbot;

class Race  {
    public Track track;
    public Car[] cars;
    public RaceSession raceSession;

    class Track {
        public String id;
        public String name;
        public Piece[] pieces;
        public Lane[] lanes;
        public StartingPoint startingPoint;
    }

    class Piece {
        public int pieceIndex;
        public double length;
        public boolean hasSwitch;
        public double radius;
        public double angle;
    }

    class Lane {
        public double distanceFromCenter;
        public int index;
    }

    class StartingPoint {
        public Position position;
        public double angle;
    }

    class Position {
        public double x, y;
    }

    class Car {
        public CarId id;
        public CarDimensions dimensions;
    }

    class CarDimensions {
        public double length, width, guideFlagPosition;
    }

    class RaceSession {
        public int laps, maxLapTimeMs;
        public boolean quickRace;
    }
}

