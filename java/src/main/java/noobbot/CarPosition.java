package noobbot;

class CarPosition {
    public CarId id;
    public double angle;
    public double velocity;
    public PiecePosition piecePosition;

    class PiecePosition {
        public int pieceIndex;
        public double inPieceDistance;
        public Lane lane;
        public int lap;
    }

    class Lane {
        public int startLaneIndex, endLaneIndex;
    }
}
