package noobbot;

class CarId {
    public String name, color;

    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof CarId)
        {
            CarId carId = (CarId) obj;
            return name.equals(carId.name) && color.equals(carId.color);
        }
        return false;
    }
}
