package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import com.google.gson.*;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        botName = args[2];
        botKey = args[3];
        if (args.length > 4)
            isDebug = true;

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        new Main();
    }

    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private static PrintWriter writer;
    private static BufferedReader reader;
    private static String botName, botKey;

    private Race race;
    private CarPosition[] carPositions;
    private CarId carId;
    private ArrayList<CarPosition> listMyPosition = new ArrayList<>();

    public Main() throws IOException {
        String line = null;
        JsonParser parser = new JsonParser();

//        send(new Join(botName, botKey));
//        send(new CreateRace(botName, botKey, "keimola", "test", 1));
        send(new CreateRace(botName, botKey, "germany", "test", 1));

        while((line = reader.readLine()) != null) {
//            debug(line);
            JsonObject msgObject = parser.parse(line).getAsJsonObject();
            String msgType = msgObject.get("msgType").getAsString();
            JsonElement msgData = msgObject.get("data");
            switch(msgType)
            {
                case "carPositions":
                    carPositions = gson.fromJson(msgData, CarPosition[].class);
                    for (CarPosition pos : carPositions) {
                        if (carId.equals(pos.id)) {
                            listMyPosition.add(pos);
                            break;
                        }
                    }
                    handleCarPositions();
                    break;

                case "gameInit":
                    race = gson.fromJson(msgData.getAsJsonObject().get("race").toString().replace("switch", "hasSwitch"), Race.class);
                    for (int i = 0, len = race.track.pieces.length; i < len; i++) {
                        Race.Piece piece = race.track.pieces[i];
                        piece.pieceIndex = i;
                        if (piece.length == 0) {
                            piece.length = Math.abs(piece.radius * 2 * Math.PI * piece.angle / 360);
                        }
                    }
                    debug(gson.toJson(race));
                    break;

                case "yourCar":
                    carId = gson.fromJson(msgData, CarId.class);

                case "join":
                case "gameStart":
                case "gameEnd":
                case "tournamentEnd":
                case "crash":
                case "spawn":
                case "lapFinished":
                case "dnf":
                case "finish":
                    log(line);
                    break;

                default:
                    send(new Ping());
                    break;
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

    private static boolean isDebug = false;

    public static CharSequence getCharSequence (Object... ao)
    {
        StringBuilder sb = new StringBuilder();
        for (Object o : ao)
        {
            sb.append(o).append(' ');
        }
        return sb;
    }

    private static void log (Object... ao)
    {
        System.out.println(getCharSequence(ao));
    }

    private static void debug (Object... ao)
    {
        if (isDebug)
            log(getCharSequence(ao));
    }

    private final static int MAX_ANGLE = 60;
    private double acceleration;
    private double lastThrottle;

    private void handleCarPositions ()
    {
        double curThrottle = 1;
        int curPos = listMyPosition.size() - 1;
        CarPosition curCarPosition = listMyPosition.get(curPos);
        CarPosition lastCarPosition = (curPos > 0) ? listMyPosition.get(curPos - 1) : new CarPosition();

        Race.Piece curPiece = race.track.pieces[curCarPosition.piecePosition.pieceIndex];
        Race.Piece nextPiece = race.track.pieces[(curCarPosition.piecePosition.pieceIndex + 1) % race.track.pieces.length];

        if (curPos == 0) {
            curCarPosition.velocity = listMyPosition.get(curPos).piecePosition.inPieceDistance;
        } else {
            if (curPos == 1) {
                acceleration = curCarPosition.piecePosition.inPieceDistance;
            }
            if (curCarPosition.piecePosition.pieceIndex == lastCarPosition.piecePosition.pieceIndex) {
                curCarPosition.velocity = curCarPosition.piecePosition.inPieceDistance - lastCarPosition.piecePosition.inPieceDistance;
            } else {
                Race.Piece lastPiece = race.track.pieces[lastCarPosition.piecePosition.pieceIndex];
                curCarPosition.velocity = lastPiece.length
                        - lastCarPosition.piecePosition.inPieceDistance
                        + curCarPosition.piecePosition.inPieceDistance;
            }

            if (curPiece.radius == 0) {
                if (nextPiece.radius == 0) {
                    curThrottle = 1;
                } else {
                    if (lastCarPosition.velocity > nextPiece.radius * 0.7 / 10){
                        curThrottle = Math.max(lastThrottle - 0.1, 0.3);
                    } else {
                        curThrottle = Math.min(lastThrottle + 0.1, 1);
                    }
                }
            } else {
                if (nextPiece.radius == 0) {
                    if (Math.abs(curCarPosition.angle) > 45) {
                        curThrottle = 0.1;
                    } else if (Math.abs(curCarPosition.angle - lastCarPosition.angle) > 1.5) {
                        curThrottle = Math.max(lastThrottle - 0.2, 0.1);
                    } else {
                        curThrottle = Math.min(lastThrottle + 0.1, 1);
                    }
                } else {
                    if (Math.abs(curCarPosition.angle) > 45) {
                        curThrottle = 0.1;
                    } else if (Math.abs(curCarPosition.angle - lastCarPosition.angle) > 1) {
                        curThrottle = Math.max(lastThrottle - 0.2, 0.1);
                    } else {
                        curThrottle = Math.min(lastThrottle + 0.05, 1);
                    }
                }
            }
        }

        curThrottle = 1;
        send(new Throttle(curThrottle));
        lastThrottle = curThrottle;

        debug("curThrottle", curThrottle,
            "  velocity", curCarPosition.velocity,
            "  velocityAngle", curCarPosition.angle - lastCarPosition.angle,
            "  angle", curCarPosition.angle,
            "  pieceIndex", curPiece.pieceIndex,
            "  inPieceDistance", curCarPosition.piecePosition.inPieceDistance,
            "  pieceLength", curPiece.length,
            "  pieceRadius", curPiece.radius
        );
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class CreateRace extends SendMsg {
    private BotId botId;
    private String trackName, password;
    private int carCount;

    public CreateRace(String botName, String botKey, String trackName, String password, int carCount) {
        botId = new BotId(botName, botKey);
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}

class JoinRace extends SendMsg {
    private BotId botId;
    private String trackName, password;
    private int carCount;

    public JoinRace(String botName, String botKey, String trackName, String password, int carCount) {
        botId = new BotId(botName, botKey);
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class BotId {
    private String name, key;

    BotId(String name, String key) {
        this.name = name;
        this.key = key;
    }
}


